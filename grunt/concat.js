//------------------------------------------------------
// Company: Peanut Hub Ltda
// Author: dmunozgaete@gmail.com
// 
// Description: Unify all into one 'big' file
// 
// URL: https://www.npmjs.com/package/grunt-contrib-concat
// 
/// NOTE: If you want to add dependdencies THIS IS THE FILE ;)!
//------------------------------------------------------
module.exports = function(grunt, options)
{

    var bower = grunt.file.readJSON('bower.json');
    var banner = grunt.template.process(grunt.file.read('banner.txt'),
    {
        data:
        {
            authors: bower.authors,
            description: bower.description,
            homepage: bower.homepage,
            version: bower.version
        }
    });

    var conf = {
        production_js:
        {
            options:
            {
                separator: ';',
                banner: banner
            },
            files:
            {
                'dist/peanuthub-custom-keyboard.js': [
                    'src/component_templates.js',
                    'src/manifiest.js',
                    'src/js/**/*.js'
                ]
            }
        },
        production_css:
        {
            options:
            {
                separator: '',
                banner: banner
            },
            files:
            {
                'dist/peanuthub-custom-keyboard.css': [
                    'src/**/*.css'
                ]
            }
        }
    };
    //---------------------------------------------------------------
    return conf;
};
