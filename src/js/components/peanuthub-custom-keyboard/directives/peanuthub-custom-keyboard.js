(function() {
    var buildCustomKeyBoard = function(keyboard)  {
        return ['$q', '$templateRequest', '$log', '$compile', '$timeout', '$rootScope', '$peanuthubCustomKeyboard', function($q, $templateRequest, $log, $compile, $timeout, $rootScope, $peanuthubCustomKeyboard) {
            return {
                restrict: 'A',
                scope: {
                    keyboardOptions: '=',
                    keyboardOnKeypress: '&',
                    keyboardOnDoneKeypress: '&',
                    keyboardOnShow: '&',
                    keyboardOnHide: '&'
                },
                transclude: false,
                controller: ['$scope', '$element', function($scope, $element) {}],
                link: function(scope, element, attributes) {

                    //var templateUrl = "bundles/peanuthub-custom-keyboard/js/components/peanuthub-custom-keyboard/peanuthub-custom-keyboard.tpl.html";
                    var templateUrl = "peanuthub-custom-keyboard/peanuthub-custom-keyboard.tpl.html";
                    var body = angular.element(document.body);

                    var input_element = element;


                    // ---------------------
                    // Model
                    var model = {
                        animation: {
                            duration: 450, //0.35s
                            slideUp: "slide-up",
                            slideDown: "slide-down"
                        },
                        doneText: "Aceptar",
                        clearText: '清空',
                        keys: ((keyboard && keyboard.keys) || null),
                        addons: ((keyboard && keyboard.addons) || null),
                        theme: "",
                    };

                    //------------------------------------------
                    // Configurations
                    (function(options) {
                        if (options.doneText) {
                            model.doneText = options.doneText.toString();
                        }

                        if (options.clearText) {
                            model.clearText = options.clearText.toString();
                        }

                        if (options.label) {
                            model.label = options.label.toString();
                        }

                        if (options.cleanTextOnClick || !angular.isUndefined(attributes.cleanTextOnClick)) {
                            model.cleanTextOnClick = true;
                        }

                        if (options.cleanTextOnFirstDeleteClick || !angular.isUndefined(attributes.cleanTextOnFirstDeleteClick)) {
                            model.cleanTextOnFirstDeleteClick = true;
                        }

                        if (options.theme) {
                            model.theme = options.theme.toString();
                        }

                        if (options.enableWAI) {
                            model.theme += " wai-enabled";
                        }

                        if (options.hideHeader) {
                            model.hideHeader = true;
                        }

                        if (!model.keys) {
                            var keyboards = $peanuthubCustomKeyboard.getKeyboards();

                            //Find the template via the options
                            if (!options.keyboard || (options.keyboard && !keyboards[options.keyboard])) {
                                throw Error("KEYBOARD_TEMPLATE_NOT_INITIALIZED");
                            }

                            var configuredKeyboard = keyboards[options.keyboard];
                            model.keys = configuredKeyboard.keys;

                            //Add the addons list to the model for rendering
                            if (configuredKeyboard && configuredKeyboard.addons) {
                                model.addons = configuredKeyboard.addons;
                            }
                        }

                    })(scope.keyboardOptions || {});
                    scope.model = model;
                    //------------------------------------------

                    //------------------------------------------
                    // Action's
                    var destroy = function(container) {
                        var defer = $q.defer();

                        //----------------------------------
                        // ANIMATION (SLIDE UP)
                        var keyboard = container.find("custom-keyboard");
                        keyboard.addClass(model.animation.slideDown);
                        var delay = $timeout(function() {
                            $timeout.cancel(delay);

                            //FIX NG-MODEL
                            //  SET READONLY FALSE
                            //  CHANGE THE VALUE OF THE INPUT
                            //  AND RE-ENABLE READONLY
                            input_element.val(scope.model.value);
                            input_element.triggerHandler("change");
                            input_element.attr("readonly", true);
                            defer.resolve();

                            //Fire Event Handler
                            var delegate = scope.keyboardOnHide();
                            if (delegate) {
                                delegate(scope.model.value);
                            }

                        }, model.animation.duration);
                        input_element.removeAttr("readonly");
                        //----------------------------------

                        defer.promise.then(function() {
                            var clonedScope = container.scope();
                            container.remove();

                            //Always call to $destroy , for destroy the
                            //addons, and call the $destroy hook
                            if (clonedScope) {
                                try {
                                    clonedScope.$destroy();
                                } catch (ex) {}
                            }


                            //Reset
                            _firstKeyPressed = true;
                        });

                        return defer.promise;
                    };

                    var _firstKeyPressed = true;
                    scope.onKeyPressed = function(ev, item) {

                        switch (item.type) {
                            case "CHAR_KEY":
                                if (_firstKeyPressed) {
                                    //Options
                                    if (model.cleanTextOnClick) {
                                        scope.model.value = '';
                                    }
                                }
                                _firstKeyPressed = false;
                                scope.model.value = scope.model.value + item.value;

                                break;
                            case "DELETE_KEY":
                                if (_firstKeyPressed && model.cleanTextOnFirstDeleteClick) {
                                    scope.model.value = '';
                                } else {
                                    var str = scope.model.value;
                                    str = str.substring(0, str.length - 1);
                                    scope.model.value = str;
                                }
                                break;
                            case "CLEAR_KEY":
                                scope.model.value = '';
                                break;
                            case "DONE_KEY":
                                //Fire Event Handler
                                var done_handler = scope.keyboardOnDoneKeypress();
                                if (done_handler) {
                                    try {
                                        done_handler(scope.model.value);
                                    } catch (ex) {}
                                }
                                break;
                        }
                        input_element.val(scope.model.value);
                        input_element.triggerHandler("change");
                        //Fire Event Handler
                        var delegate = scope.keyboardOnKeypress();
                        if (delegate) {
                            delegate(item, scope.model.value);
                        }
                    };

                    scope.onDonePressed = function(ev) {
                        var item = {
                            type: "DONE_KEY",
                            value: model.doneText
                        };
                        scope.onKeyPressed(ev, item);
                    };

                    scope.onClearPressed = function(ev) {
                        var item = {
                            type: "CLEAR_KEY",
                            value: model.clearText
                        };
                        scope.onKeyPressed(ev, item);
                    };

                    //------------------------------------------
                    var initialize = function(container) {
                        var keyboard = container.find("custom-keyboard");
                        var backdrop = container.find("custom-backdrop");
                        var doneKey = container.find("custom-keyboard-done-button");

                        scope.model.value = input_element.val();

                        backdrop.on("click", function(ev) {
                            destroy(container);
                            ev.preventDefault();
                            ev.stopPropagation();
                        });

                        doneKey.on("click", function(ev) {
                            destroy(container);
                        });

                        //----------------------------------
                        // ANIMATION (SLIDE UP)
                        keyboard.addClass(model.animation.slideUp);
                        var delay = $timeout(function() {
                            $timeout.cancel(delay);

                            keyboard.removeClass(model.animation.slideUp);

                            //Fire Event Handler
                            var delegate = scope.keyboardOnShow();
                            if (delegate) {
                                delegate(input_element.val());
                            }


                            var api = {
                                sendKeys: function(text) {
                                    scope.onKeyPressed({}, {
                                        type: "CHAR_KEY",
                                        value: text
                                    });
                                },
                                clear: function() {
                                    scope.onClearPressed({});
                                },
                                hide: function() {
                                    destroy(container);
                                },
                                done: function() {
                                    scope.onDonePressed({});
                                    destroy(container);
                                }
                            };
                            scope.$broadcast("custom-keyboard-initialize", api);

                        }, model.animation.duration);
                        //----------------------------------
                    };


                    var last_container = null;
                    $rootScope.$on("$ionicView.beforeEnter", function(event, data) {
                        // handle event
                        if (last_container) {
                            destroy(last_container);
                            last_container = null;
                        }
                    });


                    input_element.attr("readonly", true);
                    input_element.on("click", function(ev) {
                        $templateRequest(templateUrl).then(function(html) {
                            //Add Keyboard Template
                            var template = angular.element(html);


                            //Add Addons to the Template
                            var addons = template.find("custom-keyboard-addons");
                            if (model.addons) {
                                angular.forEach(model.addons, function(addon) {
                                    var elm = angular.element('<' + addon.directive + '/>');

                                    if (addon.parameters) {
                                        for (var name in addon.parameters) {
                                            elm.attr(name, addon.parameters[name]);
                                        }
                                    }

                                    addons.append(elm);
                                });
                            }

                            body.append(template);

                            var newChildScope = scope.$new();
                            $compile(template)(newChildScope);

                            //BootStrap
                            last_container = template;
                            initialize(template);
                        });

                        //Simulate Effect
                        if (model.cleanTextOnClick) {
                            scope.model.value = '';
                        }

                    });

                }
            };
        }];
    };


    angular.module('peanuthub-custom-keyboard.components')
        .directive('peanuthubCustomKeyboard', buildCustomKeyBoard())
        .directive('peanuthubNumericKeyboard', buildCustomKeyBoard({
            name: "NUMERIC",
            keys: [
                { type: "CHAR_KEY", value: "1" },
                { type: "CHAR_KEY", value: "2", label: "ABC" },
                { type: "CHAR_KEY", value: "3", label: "DEF" },
                { type: "CHAR_KEY", value: "4", label: "GHI" },
                { type: "CHAR_KEY", value: "5", label: "JKL" },
                { type: "CHAR_KEY", value: "6", label: "MNO" },
                { type: "CHAR_KEY", value: "7", label: "PQRS" },
                { type: "CHAR_KEY", value: "8", label: "TUV" },
                { type: "CHAR_KEY", value: "9", label: "WXYZ" },
                { type: "DUMMY_KEY", value: "" },
                { type: "CHAR_KEY", value: "0" },
                { type: "DELETE_KEY", icon: "ion-backspace-outline" }
            ]
        }));
})();
