angular.module('peanuthub-custom-keyboard.components')

.directive('ionNativeClick', function() {
    return {
        restrict: 'A',
        controller: function($scope, $element) {},
        link: function(scope, element, attributes) {

            element.on("click", function(ev) {
                if (ionic.Platform.isWebView() &&
                    typeof nativeclick !== "undefined") {
                    nativeclick.trigger();
                }
            });
        }
    };
});
